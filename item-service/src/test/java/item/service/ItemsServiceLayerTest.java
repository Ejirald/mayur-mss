package item.service;

import core.dao.ItemDao;
import core.dao.PropertyDao;
import core.dto.Item;
import core.dto.Property;
import item.ItemApp;
import item.repository.ItemRepository;
import item.repository.PropertyRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = ItemApp.class)
public class ItemsServiceLayerTest {

    @Autowired
    private ItemsService service;
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private PropertyRepository propertyRepository;

    private ItemDao item;
    private PropertyDao property;

    @Before
    public void prepare() {
        final ItemDao build = new ItemDao(Item.builder()
                .caption("IWatch")
                .properties(emptyList())
                .build());
        item = itemRepository.save(build);

        final PropertyDao propDao = new PropertyDao(Property
                .builder()
                .itemId(item.getId())
                .propTypeId(1000L) // WEIGHT
                .propValue("Silver")
                .build());
        property = propertyRepository.save(propDao);
    }

    @Test
    public void findOneById() {
        final Item foundItem = service.findOneById(item.getId()).block();
        assertNotNull(foundItem);
        final PropertyDao propertyDao = foundItem.getProperties().stream().findAny().get();
        assertEquals(propertyDao, property);
    }

    @Test
    public void findAll() {
        final Item foundItem = service.findAll()
                .filter(it -> it.getId().equals(item.getId())).blockFirst();
        assertNotNull(foundItem);
        final PropertyDao propertyDao = foundItem.getProperties().stream().findAny().get();
        assertEquals(propertyDao, property);
    }
}