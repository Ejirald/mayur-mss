package item.controller;

import core.dto.Item;
import item.service.ItemsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(path = "/item")
public class ItemController {

    private final ItemsService service;

    @Autowired
    public ItemController(final ItemsService service) {
        this.service = service;
    }

    @GetMapping(value = "/id")
    public Mono<Item> findItem(
            @RequestHeader("userToken") final String unused,
            @RequestParam("itemId") final Long itemId
    ) {
        return service.findOneById(itemId);
    }

    @GetMapping(value = "/all")
    public Flux<Item> findAll(
            @RequestHeader("userToken") final String unused
    ) {
        return service.findAll();
    }
}
