package item.service;

import core.dto.Item;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ItemsService {
    Mono<Item> findOneById(Long id);
    Flux<Item> findAll();
}
