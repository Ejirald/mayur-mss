package item.service;

import core.dto.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import item.repository.ItemRepository;

import static reactor.core.publisher.Mono.empty;
import static reactor.core.publisher.Mono.just;

@Service
public class ItemsServiceLayer implements ItemsService {

    private final MssLogger logger;
    private final ItemRepository repository;

    @Autowired
    public ItemsServiceLayer(final MssLogger logger,
                             final ItemRepository repository){
        this.logger = logger;
        this.repository = repository;
    }

    @Override
    @Transactional(readOnly = true)
    public Mono<Item> findOneById(final Long id) {
        if (id == null) return empty();
        try {
            return just(repository.findById(id).map(it -> (Item) it).get());
        } catch (Exception e) {
            logger.error(e);
            return empty();
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Flux<Item> findAll() {
        try {
            return Flux.fromIterable(repository.findAll());
        } catch (Exception e) {
            logger.error(e);
            return Flux.empty();
        }
    }
}
