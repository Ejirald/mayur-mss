package item;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan({"config", "core", "item"})
@EnableJpaRepositories(
        entityManagerFactoryRef = "PgEntityManagerFactory",
        transactionManagerRef = "PgTransactionManager"
)
@SpringBootApplication
public class ItemApp {
    public static void main(String[] args) {
        SpringApplication.run(ItemApp.class, args);
    }
}
