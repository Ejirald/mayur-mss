package item.service;

public interface MssLogger {
    void info(String infoMessage);
    void error(String errorMessage);
    void error(Exception exception);
}
