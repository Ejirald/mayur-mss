package item.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public final class MssLoggerLayer implements MssLogger {
    @Override
    public void info(final String infoMessage) {
        log.info(infoMessage);
    }

    @Override
    public void error(final String errorMessage) {
        log.error(errorMessage);
    }

    @Override
    public void error(final Exception exception) {
        log.error(exception.getMessage());
    }
}
