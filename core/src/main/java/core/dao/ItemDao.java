package core.dao;

import core.dto.Item;
import core.utils.GsonUtils;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.util.List;

import static java.util.Objects.requireNonNull;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;

@Entity(name = "item")
@Table(name = "item", schema = "items")
@SequenceGenerator(
        name = "itemSeq"
        , schema = "items"
        , sequenceName = "itemSeq"
        , initialValue = 1000
        , allocationSize = 100)
@AllArgsConstructor
public class ItemDao extends Item {

    public ItemDao(final Item item) {
        requireNonNull(item);
        this.setId(item.getId());
        this.setCaption(item.getCaption());
        this.setProperties(item.getProperties());
    }

    @Override
    @Id
    @GeneratedValue(generator = "itemSeq")
    public Long getId() {
        return super.getId();
    }

    @Override
    @Column(name = "caption")
    public String getCaption() { return super.getCaption(); }

    @Override
    @OneToMany(cascade = ALL, fetch = EAGER)
    @BatchSize(size = 2)
    @JoinColumn(name = "itemId")
    public List<PropertyDao> getProperties() {
        return super.getProperties();
    }

    @Override
    public String toString() {
        return GsonUtils.INSTANCE.toJson(this);
    }
}
