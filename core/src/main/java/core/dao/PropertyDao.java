package core.dao;

import core.dto.Property;
import core.utils.GsonUtils;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "properties")
@Table(name = "properties", schema = "items")
@SequenceGenerator(
        name = "propertiesSeq"
        , schema = "items"
        , sequenceName = "propertiesSeq"
        , initialValue = 1000
        , allocationSize = 300)
@AllArgsConstructor
public class PropertyDao extends Property {

    public PropertyDao(final Property property) {
        Objects.requireNonNull(property);
        this.setId(property.getId());
        this.setItemId(property.getItemId());
        this.setPropTypeId(property.getPropTypeId());
        this.setPropValue(property.getPropValue());
    }

    @Override
    @Id
    @GeneratedValue(generator = "propertiesSeq")
    public Long getId() {
        return super.getId();
    }

    @Override
    @Column(name = "itemId")
    public Long getItemId() { return super.getItemId(); }

    @Override
    @Column(name = "type")
    public Long getPropTypeId() { return super.getPropTypeId(); }

    @Override
    @Column(name = "value")
    public String getPropValue() { return super.getPropValue(); }

    @Override
    public String toString() {
        return GsonUtils.INSTANCE.toJson(this);
    }
}
