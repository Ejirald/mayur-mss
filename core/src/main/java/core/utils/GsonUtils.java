package core.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public final class GsonUtils {

    public static final Gson INSTANCE =
            new GsonBuilder().setPrettyPrinting().create();

    private GsonUtils(){}
}
