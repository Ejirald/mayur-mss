package core.dto;

import core.dao.PropertyDao;
import core.utils.GsonUtils;
import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class Item {
    private Long id;
    private String caption;
    private List<PropertyDao> properties;

    @Override
    public String toString() {
        return GsonUtils.INSTANCE.toJson(this);
    }
}
