package core.dto;

import core.utils.GsonUtils;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class Property {
    private Long id;
    private Long itemId;
    private Long propTypeId;
    private String propValue;
    @Override
    public String toString() {
        return GsonUtils.INSTANCE.toJson(this);
    }
}
