
CREATE SCHEMA IF NOT EXISTS items;

CREATE TABLE IF NOT EXiSTS items.item (
    id BIGINT PRIMARY KEY NOT NULL,
    caption VARCHAR(15) NOT NULL DEFAULT ''
);

CREATE SEQUENCE IF NOT EXISTS items.itemSeq
    INCREMENT BY 100
    START WITH 1000
    OWNED BY items.item.id;

CREATE TABLE IF NOT EXISTS items.propType (
   id BIGINT PRIMARY KEY NOT NULL,
   caption VARCHAR(10) NOT NULL DEFAULT ''
);

CREATE SEQUENCE IF NOT EXISTS items.propTypeSeq
    START WITH 1000
    OWNED BY items.propType.id;

CREATE TABLE IF NOT EXISTS items.properties (
    id BIGINT PRIMARY KEY NOT NULL,
    itemID BIGINT REFERENCES items.item (id),
    type BIGINT REFERENCES items.propType (id),
    value VARCHAR(30)
);

CREATE INDEX IF NOT EXISTS properties_item_id ON items.properties (itemID);
CREATE INDEX IF NOT EXISTS properties_type ON items.properties (type);

CREATE SEQUENCE IF NOT EXISTS items.propertiesSeq
    START WITH 1000
    INCREMENT BY 300
    OWNED BY items.properties.id;

DO $INIT_ITEMS$
    DECLARE
        __id BIGINT;
        __weight_type_id BIGINT;
        __color_type_id BIGINT;
    BEGIN
        INSERT INTO items.propType (id, caption)
        VALUES (nextval('items.propTypeSeq'), 'WEIGHT') returning id INTO __weight_type_id;
        INSERT INTO items.propType (id, caption)
        VALUES (nextval('items.propTypeSeq'), 'COLOR') returning id INTO __color_type_id;

        INSERT INTO items.item (id, caption)
        VALUES (nextval('items.itemSeq'), 'IPad') RETURNING id INTO __id;

        INSERT INTO items.properties (id, itemID, type, value)
        VALUES (nextval('items.propertiesSeq'), __id, __weight_type_id, '350');
        INSERT INTO items.properties (id, itemID, type, value)
        VALUES (nextval('items.propertiesSeq'), __id, __color_type_id, 'Black');

        INSERT INTO items.item (id, caption)
        VALUES (nextval('items.itemSeq'), 'IPhone') RETURNING id INTO __id;

        INSERT INTO items.properties (id, itemID, type, value)
        VALUES (nextval('items.propertiesSeq'), __id, __weight_type_id, '300');
        INSERT INTO items.properties (id, itemID, type, value)
        VALUES (nextval('items.propertiesSeq'), __id, __color_type_id, 'Gray');
    END;
$INIT_ITEMS$;

