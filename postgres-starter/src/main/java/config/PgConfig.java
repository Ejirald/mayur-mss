package config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ConfigurationProperties("mss.db")
public class PgConfig {
    private Boolean container = true;
    private Boolean migrate = true;
    private Boolean autocommit = true;
    private String driverClassName = "org.postgresql.Driver";
    private String image = "postgres:11.4";
    private String username = "postgres";
    private String password = "postgres";
    private String poolName = "MSS_PG_POOL";
    private Integer poolSize = 10;
}
