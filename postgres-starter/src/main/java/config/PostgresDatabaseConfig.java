package config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.testcontainers.containers.PostgreSQLContainer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;


@Configuration
@EnableConfigurationProperties(PgConfig.class)
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "PgEntityManagerFactory",
        transactionManagerRef = "PgTransactionManager"
)
public class PostgresDatabaseConfig {

    @Primary
    @Bean(name = "PgDataSource")
    public DataSource dataSource(@Autowired final PgConfig pgConf) {
        final HikariConfig config = new HikariConfig();
        config.setAutoCommit(pgConf.getAutocommit());
        config.setDriverClassName(pgConf.getDriverClassName());
        config.setUsername(pgConf.getUsername());
        config.setPassword(pgConf.getPassword());
        config.setPoolName(pgConf.getPoolName());
        config.setMaximumPoolSize(pgConf.getPoolSize());
        if (pgConf.getContainer()) {
            final PostgreSQLContainer container =
                    new PostgreSQLContainer(pgConf.getImage()) {{
                            withDatabaseName(pgConf.getUsername());
                            withUsername(pgConf.getUsername());
                            withPassword(pgConf.getPassword());
                    }};
            container.start();
            config.setJdbcUrl(container.getJdbcUrl());
        }
        final HikariDataSource hikariDataSource = new HikariDataSource(config);
        if (pgConf.getMigrate()) {
            final Flyway flyway = new Flyway() {{
                setDataSource(hikariDataSource);
                setBaselineOnMigrate(true);
            }};
            flyway.migrate();
        }
        return hikariDataSource;
    }

    @Primary
    @Bean(name = "PgEntityManager")
    public EntityManager entityManager(@Autowired final EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

    @Bean(name = "PgEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @Autowired final JpaVendorAdapter jpaVendorAdapter,
            @Autowired @Qualifier("PgDataSource") final DataSource dataSource
    ) {
        final LocalContainerEntityManagerFactoryBean em
                = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan("core");
        em.setJpaVendorAdapter(jpaVendorAdapter);
        return em;
    }

    @Bean(name = "PgTransactionManager")
    public PlatformTransactionManager transactionManager(@Autowired final EntityManagerFactory emf) {
        return new JpaTransactionManager(emf);
    }
}